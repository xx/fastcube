# Package

version       = "0.1.0"
author        = "Rasmus Moorats"
description   = "ClassiCube server software"
license       = "MIT"
srcDir        = "src"
bin           = @["fastcube"]


# Dependencies

requires "nim >= 2.0.0"
requires "print >= 1.0.2"
requires "flatty >= 0.3.4"
requires "zippy >= 0.10.10"
requires "https://github.com/status-im/nim-chronicles.git#head"
