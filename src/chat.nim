import std/[asyncnet, asyncdispatch, strutils]
import datatypes, player, outbound

type
  MessageType* = enum
    NormalM, WorldM, WhisperM, CommandM

proc getType*(msg: string): MessageType =
  case msg[0]:
    of '/':  CommandM
    of '!':  WorldM
    of '\'': WhisperM
    else:    NormalM

iterator splitMsg(msg: string, max = StringMax): string =
  var
    color = '\0'
    buf: string
  
  for i, c in msg:
    if c == '&' and i != msg.len - 1:
      case msg[i + 1]:
        of '0' .. 'e':
          color = msg[i + 1]
        of 'f':
          color = '\0'
        else: discard
    buf.add c

    if buf.len == max and i != msg.len - 1:
      yield buf
      buf = "> "
      if color != '\0':
        buf.add "&" & color

  yield buf

proc sendMsg*(players: seq[Player], msg: string) {.async.} =
  for chunk in msg.splitMsg:
    let
      sanitized = chunk.strip(leading = false, chars = {'&'})
      msgPacked = outbound.Message(playerId: -1, message: packString sanitized).pack

    for player in valid players:
      await player.socket.send msgPacked

proc sendMsg*(player: Player, msg: string) {.async.} =
  for chunk in msg.splitMsg:
    let
      sanitized = chunk.strip(leading = false, chars = {'&'})
      msgPacked = outbound.Message(playerId: -1, message: packString sanitized).pack

    await player.socket.send msgPacked
