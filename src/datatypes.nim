import std/strutils

#[
  Type       Bytes Description
  ----------|-----|-----------
  Byte       1     Unsigned integer (0 to 255)
  SByte      1     Signed integer (-128 to 127)
  FByte      1     Signed fixed-point, 5 fractional bits (-4 to 3.96875)
  Short      2     Signed integer (-32768 to 32767)
  FShort     2     Signed fixed-point, 5 fractional bits (-1024 to 1023.96875)
  String     64    US-ASCII/ISO646-US encoded string padded with spaces (0x20)
  Byte array 1024  Binary data padded with null bytes (0x00)
]#

const
  StringMax*  = 64
  BArrayMax*  = 1024
  PlayerSelf* = -1.int8

type
  Byte*      = uint8
  SByte*     = int8
  FByte*     = int8
  Short*     = int16
  FShort*    = int16
  String*    = string
  ByteArray* = seq[Byte]

func unpackString*(s: string): String =
  result = s
  result.removeSuffix ' '

func packString*(s: String): string =
  result =
    if s.len > StringMax: s[0 ..< StringMax]
    else: s & ' '.repeat(StringMax - s.len)

func toFloat*(input: FByte | FShort): float32 =
  input.float32 / 32.0

func toFByte*(input: float32): FByte =
  FByte(input * 32.0)

func toFShort*(input: float32): FShort =
  FShort(input * 32.0)

converter toShort*(input: uint16): Short =
  cast[Short](input)

converter toUint16*(input: Short): uint16 =
  cast[uint16](input)
