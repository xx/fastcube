import std/[asyncfile, asyncdispatch, os, sequtils, sets, strformat]
import datatypes
import flatty/binny, zippy
import chronicles

const
  Magic: array[4, char] = ['F', 'C', 'L', '1']
  ChunkSize = 1024
  NameSize  = 64 # size of name of level when saved

type
  LevelSpec = object
    name*:      string
    file*:      string
    size_x*:    uint16
    size_y*:    uint16
    size_z*:    uint16
    spawn_x*:   float32
    spawn_y*:   float32
    spawn_z*:   float32
    blocks*:    seq[uint8]
    dirty:      HashSet[uint]
    meta_dirty: bool
    fd:         AsyncFile

  BinaryLevelHeader {.packed.} = object
    magic: array[sizeof Magic, char]  # "FCL1"
    blocks_start: uint16              # file offset where block data starts
    name:    array[NameSize, uint8]   # level name itself
    size_x:  uint16                   # block data size = x * y * z
    size_y:  uint16
    size_z:  uint16
    spawn_x: float32
    spawn_y: float32
    spawn_z: float32

  Level* = ref LevelSpec

proc worldDir*(): string =
  # when defined(release):
  # todo: implement
  getTempDir() / "fastcube"

proc toPath*(name: string): string =
  worldDir() / &"{name}.fcl"

proc save*(lvl: Level) {.async.} =
  ## Perform a full save of all data
  let dir = splitPath(lvl.file)[0]
  if not dirExists dir:
    createDir dir

  var name: array[NameSize, uint8]
  copyMem(addr name[0], addr lvl.name[0], lvl.name.len)
  let header = BinaryLevelHeader(
    magic: Magic,
    name:  name,
    blocks_start: (sizeof BinaryLevelHeader).uint16,
    size_x:  lvl.size_x,
    size_y:  lvl.size_y,
    size_z:  lvl.size_z,
    spawn_x: lvl.spawn_x,
    spawn_y: lvl.spawn_y,
    spawn_z: lvl.spawn_z,
  )

  if lvl.fd == nil:
    lvl.fd = lvl.file.openAsync fmReadWrite
  else:
    lvl.fd.setFilePos 0

  await lvl.fd.writeBuffer(addr header, header.blocks_start.int)

  let blocks_size = lvl.size_x.uint * lvl.size_y.uint * lvl.size_z.uint
  await lvl.fd.writeBuffer(addr lvl.blocks[0], blocks_size.int)

proc close*(lvl: Level) {.async.} =
  await lvl.save
  if lvl.fd != nil:
    close lvl.fd

proc load*(file: string): Future[Level] {.async.} =
  ## Load a level from a file
  if not file.fileExists:
    raise newException(IOError, "no such file")

  var lvl = Level()
  let
    minSize = sizeof(BinaryLevelHeader)
  lvl.fd = file.openAsync fmReadWriteExisting

  if lvl.fd.getFileSize < minSize:
    error "file smaller than minimum allowed,", minSize, filesize = lvl.fd.getFileSize
    return lvl
  var fHeader: BinaryLevelHeader
  discard await lvl.fd.readBuffer(addr fHeader, minSize)

  let total = fHeader.size_x.uint * fHeader.size_y.uint * fHeader.size_z.uint

  if lvl.fd.getFileSize < int(fHeader.blocks_start + total):
    error "file smaller than block data indicates", filesize = lvl.fd.getFileSize, indicated = int(fHeader.blocks_start + total)
    return lvl

  lvl.name    = $cast[cstring](addr fHeader.name[0])
  lvl.file    = file
  lvl.size_x  = fHeader.size_x
  lvl.size_y  = fHeader.size_y
  lvl.size_z  = fHeader.size_z
  lvl.spawn_x = fHeader.spawn_x
  lvl.spawn_y = fHeader.spawn_y
  lvl.spawn_z = fHeader.spawn_z

  lvl.blocks.setLen total

  lvl.fd.setFilePos fHeader.blocks_start.int64
  discard await lvl.fd.readBuffer(addr lvl.blocks[0], total.int)

  debug "loading level from disk", name = lvl.name, file = lvl.file

  return lvl

proc createFlatgrass*(name: string, size_x, size_y, size_z: uint16): Future[Level] {.async.} =
  let lvl = Level(
    name:    name,
    file:    name.toPath,
    size_x:  size_x,
    size_y:  size_y,
    size_z:  size_z,
    spawn_x: size_x.float32 / 2,
    spawn_y: size_y.float32 / 2 + 2,
    spawn_z: size_z.float32 / 2,
  )

  let half = size_y div 2

  for y in 0'u16 ..< size_y:
    for z in 0'u16 ..< size_z:
      for x in 0'u16 ..< size_x:
        let blockType: uint8 =
          if y < half:    1 # stone
          elif y == half: 2 # grass
          else:           0 # air
        lvl.blocks.add blockType

  await lvl.save
  return lvl

proc gzip*(lvl: Level): seq[uint8] =
  let len = lvl.blocks.len.uint32.swap
  var lenStr = ""
  lenStr.addUint32 len
  (cast[seq[uint8]](lenStr) & lvl.blocks).compress(BestSpeed, dfGzip)

iterator chunk*(gzippedLvl: seq[uint8]): (int, ByteArray) =
  var i = ChunkSize
  while i < gzippedLvl.len:
    yield (ChunkSize, gzippedLvl[i - ChunkSize ..< i])
    i += ChunkSize

  let tail = gzippedLvl[i - ChunkSize .. ^1]
  yield (tail.len, tail & repeat(0x00.uint8, ChunkSize - tail.len))

func flatIndex*(lvl: Level, x, y, z: uint16): uint =
  y.uint * (lvl.size_z.uint * lvl.size_x.uint) + z.uint * lvl.size_x.uint + x.uint

func blockAt*(lvl: Level; x, y, z: uint16): (bool, uint8) =
  # gets block id at xyz. returns true if ok, false if not
  if x >= lvl.size_x or y >= lvl.size_y or z >= lvl.size_z:
    return (false, 0)

  let index = lvl.flatIndex(x, y, z)

  return (true, lvl.blocks[index])

proc setBlockAt*(lvl: Level; x, y, z: uint16; id: uint8): (bool, uint8) =
  # sets block id at xyz. returns true if ok, false if not, and the old block id
  if x >= lvl.size_x or y >= lvl.size_y or z >= lvl.size_z:
    return (false, 0)

  let
    index = lvl.flatIndex(x, y, z)
    old = lvl.blocks[index]

  lvl.dirty.incl index
  lvl.blocks[index] = id

  return (true, old)

