import std/[asyncdispatch, asyncnet, math, strformat]
import chat, datatypes, level, loadedlevel, outbound, player
import chronicles

proc loadLevel*(player: Player, lvl: LoadedLevel) {.async.} =
  logScope:
    player = player.name
    level = lvl.lvl.name
  info "loading level for player"
  let 
    client = player.socket
    map = lvl.lvl
  let serverIdent = ServerIdent(
    protoVersion: 0x07,
    name:         "fastcube",
    motd:         &"loading world {map.name}",
    userType:     0x00
  )

  await client.send serverIdent.pack

  let levelInit = LevelInit()
  await client.send levelInit.pack

  let gzipped = map.gzip
  var sent = 0
  for (len, data) in gzipped.chunk:
    debug "sending map chunk data", len
    sent += len
    let
      percent = ceil((sent / gzipped.len) * 100).Byte
      levelDataChunk = LevelDataChunk(length: len.Short, data: data, percent: percent)
      packed = levelDataChunk.pack
    await client.send packed

  let levelFinalize = LevelFinalize(
    xSize: map.size_x.int16,
    ySize: map.size_y.int16,
    zSize: map.size_z.int16,
  )
  await client.send levelFinalize.pack

  let setPos = SetPos(
    playerId: PlayerSelf,
    x: toFShort map.spawn_x,
    y: toFShort map.spawn_y,
    z: toFShort map.spawn_z,
  )
  await client.send setPos.pack

  let
    joinPacked = SpawnPlayer(
      playerId: player.id.SByte,
      playerName: player.name,
      x: toFShort map.spawn_x,
      y: toFShort map.spawn_y,
      z: toFShort map.spawn_z,
    ).pack

    selfJoin = SpawnPlayer(
      playerId: PlayerSelf,
      playerName: player.name,
      x: toFShort map.spawn_x,
      y: toFShort map.spawn_y,
      z: toFShort map.spawn_z,
    )

  await client.send selfJoin.pack

  for lvlPlayer in lvl.players:
    await lvlPlayer.sendMsg &"{player.rank}{player.name}&e joined &f{map.name}"

    if lvlPlayer.id != player.id:
      let
        playerAdd = SpawnPlayer(
          playerId:   lvlPlayer.id.int8,
          playerName: lvlPlayer.name,
          x:          lvlPlayer.pos.x,
          y:          lvlPlayer.pos.y,
          z:          lvlPlayer.pos.z,
        ).pack

        playerPos = SetPos(
          playerId: lvlPlayer.id.int8,
          x:        lvlPlayer.pos.x,
          y:        lvlPlayer.pos.y,
          z:        lvlPlayer.pos.z,
          yaw:      lvlPlayer.pos.yaw,
          pitch:    lvlPlayer.pos.pitch,
        ).pack

      await client.send playerAdd
      await client.send playerPos
      await lvlPlayer.socket.send joinPacked

proc unloadLevel*(player: Player, lvl: LoadedLevel, notify = true) {.async.} =
  let
    despawnPacked = DespawnPlayer(playerId: player.id.SByte).pack
    map = lvl.lvl

  for lvlPlayer in lvl.players:
    if lvlPlayer.id == player.id: continue
    await lvlPlayer.socket.send despawnPacked
    if notify:
      await lvlPlayer.sendMsg &"{player.rank}{player.name}&e left &f{map.name}"
