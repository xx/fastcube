import datatypes
import flatty/binny

type
  ServerIdent* = object
    # Response to a joining player. The user type indicates whether a player is
    # an op (0x64) or not (0x00) which decides if the player can delete bedrock.
    # The protocol version is 0x07, unless you're using a client below 0.28.
    protoVersion*: Byte
    name*, motd*: String
    userType*: Byte

  Ping = object
    # Sent to clients periodically. The only way a client can disconnect at the
    # moment is to force it closed, which does not let the server know. The ping
    # packet is used to determine if the connection is still open.

  LevelInit* = object
    # Notifies the player of incoming level data.

  LevelDataChunk* = object
    # Contains a chunk of the gzipped level data, to be concatenated with the
    # rest. Chunk Data is up to 1024 bytes, padded with 0x00s if less.
    length*: Short
    data*: ByteArray
    percent*: Byte

  LevelFinalize* = object
    # Sent after level data is complete and gives map dimensions.
    # The y coordinate is how tall the map is.
    xSize*, ySize*, zSize*: Short

  SetBlock* = object
    # Sent to indicate a block change by physics or by players. In the case of a
    # player change, the server will also echo the block change back to the
    # player who initiated it.
    x*, y*, z*: Short
    blockType*: Byte

  SpawnPlayer* = object
    # Sent to indicate where a new player is spawning in the world. This will
    # set the player's spawn point.
    playerID*: SByte
    playerName*: String
    x*, y*, z*: FShort
    yaw*, pitch*: Byte

  SetPos* = object
    # Sent with changes in player position and rotation. Used for sending
    # initial position on the map, and teleportation.
    # Some servers don't send relative packets, opting to only use this one.
    playerId*: SByte
    x*, y*, z*: FShort
    yaw*, pitch*: Byte

  PosOrientationUpdate = object
    # Sent with changes in player position and rotation. Sent when both position
    # and orientation is changed at the same time.
    # Not required for server operation.
    playerId: SByte
    x, y, z: FByte
    yaw, pitch: Byte

  PosUpdate = object
    # Sent with changes in player position.
    # Not required for server operation.
    playerId: SByte
    x, y, z: FByte

  OrientationUpdate = object
    # Sent with changes in player rotation.
    # Not required for server operation.
    playerId: SByte
    yaw, pitch: Byte

  DespawnPlayer* = object
    # Sent to others when the player disconnects.
    playerId*: SByte

  Message* = object
    # Messages sent by chat or from the console.
    playerId*: SByte
    message*: String

  DisconnectPlayer = object
    # Sent to a player when they're disconnected from the server.
    reason: String

  UpdateUserType = object
    # Sent when a player is opped/deopped. User type is 0x64 for op, 0x00 for
    # normal user. This decides if the player can delete bedrock.
    userType: Byte

func pack*(p: ServerIdent | Ping | LevelInit | LevelDataChunk | LevelFinalize |
              SetBlock | SpawnPlayer | SetPos | PosOrientationUpdate |
              PosUpdate | OrientationUpdate | DespawnPlayer | Message |
              DisconnectPlayer | UpdateUserType): string =

  result.add((
    when p.type is          ServerIdent: 0x00
    elif p.type is                 Ping: 0x01
    elif p.type is            LevelInit: 0x02
    elif p.type is       LevelDataChunk: 0x03
    elif p.type is        LevelFinalize: 0x04
    elif p.type is             SetBlock: 0x06
    elif p.type is          SpawnPlayer: 0x07
    elif p.type is               SetPos: 0x08
    elif p.type is PosOrientationUpdate: 0x09
    elif p.type is            PosUpdate: 0x0A
    elif p.type is    OrientationUpdate: 0x0B
    elif p.type is        DespawnPlayer: 0x0C
    elif p.type is              Message: 0x0D
    elif p.type is     DisconnectPlayer: 0x0E
    elif p.type is       UpdateUserType: 0x0F
  ).chr)

  for key, val in p.fieldPairs:
    when val.type is String:
      result.add val.packString
    elif val.type is Byte:
      result.add val.chr
    elif val.type is SByte:
      result.add cast[uint8](val).chr
    elif val.type is Short or val.type is FShort:
      result.addUint16(cast[uint16](val).swap)
    elif val.type is ByteArray:
      result.add cast[string](val)
    else:
      result.add val
