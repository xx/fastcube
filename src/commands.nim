import std/[asyncdispatch, enumutils, strutils, strformat, tables, os]
import chat, datatypes, level, context, player, loadedlevel, levelnet, version
import chronicles

type
  CommandResult = object
    failed*:  bool
    msg*:     string

  Command = object
    name:        string
    description: string
    aliases:     seq[string]
    usage:       string
    requires:    Rank
    argsMinMax:  (int, int)
    cb: proc(args: seq[string], ctx: Context): Future[CommandResult]

var registeredCommands = initTable[string, Command]()

proc register(cmd: Command) =
  registeredCommands[cmd.name] = cmd
  for alias in cmd.aliases:
    registeredCommands[alias] = cmd

  debug "registered command", name = cmd.name

proc handleCommand*(cmd: string, ctx: Context) {.async.} =
  let args = split(cmd[1..^1], ' ')
  # todo: if '/', repeat last command
  if args.len < 1 or args[0] == "": return

  logScope:
    player = ctx.player.name

  if not registeredCommands.hasKey args[0]:
    await ctx.player.sendMsg &"&eno such command: &f/{args[0]}"
    info "attempted to use invalid command", command = args[0]
    return

  let
    command = registeredCommands[args[0]]
    (argsMin, argsMax) = command.argsMinMax

  logScope:
    command = command.name
    args = args

  if not ctx.player.hasRank command.requires:
    await ctx.player.sendMsg &"&eyou're not allowed to use &f/{args[0]}"
    info "attempted to use unpermitted command"
    return

  if argsMin != -1 and args.len < argsMin:
    await ctx.player.sendMsg &"&etoo few arguments (min: &f{argsMin}&e)"
    return

  if argsMax != -1 and args.len > argsMax:
    await ctx.player.sendMsg &"&etoo many arguments (max: &f{argsMax}&e)"
    return

  try:
    info "handling command"
    let res = await command.cb(args, ctx)
    if res.failed:
      if res.msg != "":
        debug "command failed", msg = res.msg
        await ctx.player.sendMsg &"&e{res.msg}"
      else:
        error "command errored but returned no msg!"
        await ctx.player.sendMsg "&ecommand failed with no msg. bug!"
  except CatchableError:
    error "command error", error = getCurrentExceptionMsg()
    await ctx.player.sendMsg &"&eserver exception: &f{getCurrentExceptionMsg()}"

proc cbHelp(args: seq[string], ctx: Context): Future[CommandResult] {.async.} =
  if args.len == 1:
    var shown: seq[string]
    await ctx.player.sendMsg "&elist of commands:"
    for _, cmd in registeredCommands:
      if cmd.name in shown: continue
      await ctx.player.sendMsg &"&e/&f{cmd.usage}"
      await ctx.player.sendMsg &"  &e{cmd.description}"
      shown.add cmd.name
    return

  if args[1] notin registeredCommands:
    return CommandResult(failed: true, msg: &"no such command: &f/{args[1]}")

  let cmd = registeredCommands[args[1]]

  await ctx.player.sendMsg &"&ecommand: &f{cmd.name}"
  await ctx.player.sendMsg &"&edescription: &f{cmd.description}"
  await ctx.player.sendMsg &"&erequires: {cmd.requires}{cmd.requires.symbolName}"
  await ctx.player.sendMsg &"&eusage: &f/{cmd.usage}"
  if cmd.aliases.len != 0:
    await ctx.player.sendMsg &"&ealiases: &f" & cmd.aliases.join("&e,&f ")

register Command(
  name: "help",
  description: "shows helpful information",
  aliases: @["h", "?"],
  usage: "help [command]",
  argsMinMax: (-1, 2),
  cb: cbHelp
)

proc cbVersion(args: seq[string], ctx: Context): Future[CommandResult] {.async.} =
  await ctx.player.sendMsg &"&efastcube version: &f{buildVersion}"
  await ctx.player.sendMsg &"&ecompiled at: &f{CompileDate} {CompileTime} UTC"
  await ctx.player.sendMsg &"&eNim version: &f{NimVersion}"
  await ctx.player.sendMsg &"&erunning on: &f{hostOS} {hostCPU}"
  await ctx.player.sendMsg &"&euptime: &f{uptime()}"

register Command(
  name: "version",
  description: "shows the fastcube version",
  aliases: @["ver"],
  usage: "version",
  argsMinMax: (-1, -1),
  cb: cbVersion
)

proc sbCbBlockInfo(playerId: int, players: seq[Player], x, y, z: Short, blocktype: Byte, level: Level): Future[bool] {.async.} =
  let
    player = players[playerId]
    (ok, lvlblock) = level.blockAt(x, y, z)

  if not ok:
    await player.sendMsg "&einvalid block placement!"
    return false

  let index = level.flatIndex(x, y, z)

  await player.sendMsg &"&ex: &f{x}&e, y: &f{y}&e, z: &f{z}&e ({index}): &f{lvlblock}"
  return true

proc cbBlockInfo(args: seq[string], ctx: Context): Future[CommandResult] {.async.} =
  ctx.player.setBlockCb = sbCbBlockInfo
  await ctx.player.sendMsg "&eclick on a block to get its info"

register Command(
  name: "blockinfo",
  description: "shows block information",
  usage: "blockinfo",
  aliases: @["bi"],
  requires: Member,
  argsMinMax: (-1, -1),
  cb: cbBlockInfo
)

proc cbToggle(args: seq[string], ctx: Context): Future[CommandResult] {.async.} =
  if args.len == 1:
    ctx.player.extra = ""
    ctx.player.setBlockCb = nil
    ctx.player.toggled = false
    await ctx.player.sendMsg "&ecleared toggled command"
  else:
    let
      cmd = "/" & args[1..^1].join(" ")
    await ctx.player.sendMsg &"&etoggled command: &f{cmd}"
    await cmd.handleCommand ctx
    ctx.player.toggled = true

register Command(
  name: "toggle",
  description: "runs command on each click",
  usage: "toggle [cmd]. Leave blank to untoggle",
  aliases: @["t"],
  argsMinMax: (-1, -1),
  cb: cbToggle
)

proc cbGoto(args: seq[string], ctx: Context): Future[CommandResult] {.async.} =
  var 
    lvl: LoadedLevel

  try:
    lvl = await args[1].get
  except IOError:
    return CommandResult(failed: true, msg: &"no such level: &f{args[1]}")

  let currentLevel = await ctx.player.loadedlevel.get

  await ctx.player.unloadLevel currentLevel
  await ctx.player.sendTo lvl
  await ctx.player.loadLevel lvl
  ctx.player.loadedlevel = lvl.lvl.name

register Command(
  name: "goto",
  description: "sends to player to a world",
  usage: "goto <world name>",
  aliases: @["g"],
  argsMinMax: (2, 2),
  cb: cbGoto
)

proc cbCreateLevel(args: seq[string], ctx: Context): Future[CommandResult] {.async.} =
  if args[1].toPath.fileExists:
    return CommandResult(failed: true, msg: &"level &f{args[1]} &ealready exists!")
  let 
    worldType =
      if args.len < 6: "flat"
      else: args[5]
    x = parseInt args[2]
    y = parseInt args[3]
    z = parseInt args[4]
    # todo - add checks
    lvl =
      case worldType:
        of "flat": await createFlatgrass(args[1], x.uint16, y.uint16, z.uint16)
        else: nil

  if lvl.isNil:
    return CommandResult(failed: true, msg: &"no such world type: &f{worldType}")
  
  await ctx.players[ctx.playerId].sendMsg &"&ecreated level &f{lvl.name}"

register Command(
  name: "createlevel",
  description: "creates a new level",
  usage: "createlevel <name> <x> <y> <z> [type]",
  argsMinMax: (5, 6),
  cb: cbCreateLevel
)
