import player, level

type
  Context* = object
    players*:  seq[Player]
    playerId*: int
    level*:    Level

func player*(ctx: Context): Player =
  ctx.players[ctx.playerId]
