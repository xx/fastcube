import std/[asyncnet, asyncdispatch, strutils, strformat]
import context, chat, commands, datatypes, inbound, level, levelnet, loadedlevel, outbound, player
import chronicles

const UnsetId = -1

var players: seq[Player]

proc handlePlayerIdent(client: AsyncSocket, data: string, oid: int): Future[int] {.async.} =
  if oid != UnsetId: return oid

  let playerData = parsePlayerIdent data

  debug "handling playerident", username = playerData.username, verificationKey = playerData.verificationKey

  var id = UnsetId
  for i, k in players.pairs:
    if k.isNil:
      id = i
      break

  if id == UnsetId:
    id = players.len
    players.add nil

  let player = Player(
    id:     id.uint8,
    ip:     client.getPeerAddr[0],
    name:   playerData.username,
    socket: client,
    pos:    new player.Position,
  )

  players[id] = player

  info "player connected", player = player.name, address = player.ip
  let lvl = await player.sendToDefault

  await players.sendMsg &"{player.rank}{player.name}&e connected"
  await player.loadLevel lvl

  return id

proc handleSetBlock(client: AsyncSocket, data: string, id: int) {.async.} =
  if id == UnsetId: return
  let change = parseSetBlock data

  let
    blockType: Byte =
      if change.mode == 0: 0
      else: change.blockType
    lvl = await players[id].getLevel
    map = lvl.lvl
  var plr = players[id]
  let cb = plr.setBlockCb
  if cb != nil:
    let
      (ok, lvlblock) = map.blockAt(change.x, change.y, change.z)
      done = await cb(id, players, change.x, change.y, change.z, blockType, map)
    if done:
      plr.extra = ""
      if not plr.toggled: plr.setBlockCb = nil
    if ok:
      let setBlockPacked = outbound.SetBlock(
        x: change.x,
        y: change.y,
        z: change.z,
        blockType: lvlblock,
      ).pack
      await plr.socket.send setBlockPacked
    return

  let (success, old) = map.setBlockAt(change.x, change.y, change.z, blockType)

  debug "setting block", player = plr.name, x = change.x, y = change.y, z = change.z, blockType, old

  if not success:
    error "player attempted to place out of bounds", player = plr.name
    return

  let setBlockPacked = outbound.SetBlock(
    x: change.x,
    y: change.y,
    z: change.z,
    blockType: blockType,
  ).pack

  for player in lvl.players:
    await player.socket.send setBlockPacked

  await save map

proc handlePosition(client: AsyncSocket, data: string, id: int) {.async.} =
  if id == UnsetId: return
  let position = parsePosition data

  let player = players[id]
  if player.pos.x == position.x and player.pos.y == position.y and
   player.pos.z == position.z and player.pos.yaw == position.yaw and
   player.pos.pitch == position.pitch:
    return

  player.pos.x     = position.x
  player.pos.y     = position.y
  player.pos.z     = position.z
  player.pos.yaw   = position.yaw
  player.pos.pitch = position.pitch

  let
    setPos = outbound.SetPos(
      playerId: id.SByte,
      x:        position.x,
      y:        position.y,
      z:        position.z,
      yaw:      position.yaw,
      pitch:    position.pitch,
    ).pack
    lvl = await player.getLevel

  for player in lvl.players:
    if player.id != id.uint8:
      await player.socket.send setPos

proc handleMessage(client: AsyncSocket, data: string, id: int) {.async.} =
  if id == UnsetId: return
  let
    message = parseMessage data
    msgType = message.message.getType
    map = (await players[id].getLevel).lvl
    player = players[id]

  debug "handling message", player = player.name, message = message.message

  case msgType:
    of CommandM:
      var ctx = Context(
        players: players,
        playerId: id,
        level: map
      )
      await handleCommand(message.message, ctx)
    else:
      var msgstring = &"{player.rank}{player.name}&f: {message.message}"
      info "player sent message", player = player.name, message = message.message
      msgstring.removeSuffix '&'
      let packed = outbound.Message(playerId: id.SByte, message: msgstring).pack

      for player in valid players:
        await player.socket.send packed

proc handleDisconnect(client: AsyncSocket, id: int) {.async.} =
  client.close()
  if id == UnsetId: return

  # ensure that the player is still in a connected state
  if id > players.high or players[id].isNil: return

  let 
    player = players[id]
    map = await player.getLevel

  await player.unloadLevel(map, false)

  info "player disconnected", player = player.name
  if id == players.high:
    players.del id
  else:
    # prefer not leaving holes, but... hack
    players[id] = nil

  await players.sendMsg &"{player.rank}{player.name}&e disconnected"

proc handle(client: AsyncSocket) {.async.} =
  var id = UnsetId
  try:
    while true:
      let data = await client.recv 1024
      if data == "":
        await client.handleDisconnect id
        return
      case data[0].uint8:
        of 0x00: id = await client.handlePlayerIdent(data, id)
        of 0x05: await client.handleSetBlock(data, id)
        of 0x08: await client.handlePosition(data, id)
        of 0x0D: await client.handleMessage(data, id)
        else: error "unknown packet type", packet = $data[0].ord
  except IndexDefect:
    # todo: test if we need this
    error "indexdefect, disconnecting"
    await client.handleDisconnect id
    return

func `$`(x: Port): string =
  $x.uint16

proc start*(address: string = "0.0.0.0", port: int = 25565) {.async.} =
  var server = newAsyncSocket(buffered = false)
  server.setSockOpt(OptReuseAddr, true)
  server.bindAddr(Port(port))
  server.listen()

  info "fastcube listening", address, port

  while true:
    let
      client = await server.accept()
      (ip, port) = getPeerAddr client
    debug "new connection", address = ip, port
    asyncCheck client.handle()
