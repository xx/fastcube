import std/[asyncdispatch, os, tables]
import level, player
import chronicles

const
  Default = "default" # default world name

type
  LoadedLevelSpec = object
    lvl*: Level
    playersT: Table[int, Player]

  LoadedLevel* = ref LoadedLevelSpec

var levels* = newTable[string, LoadedLevel](1)

proc get*(lvl: Level): Future[LoadedLevel] {.async.} =
  ## Load level by Level object
  if levels.hasKey lvl.name:
    return levels[lvl.name]

  result = new LoadedLevel
  result.lvl = lvl
  levels[lvl.name] = result

proc get*(lvl: string): Future[LoadedLevel] {.async.} =
  ## Load level by name
  if levels.hasKey lvl:
    return levels[lvl]

  let level = await load lvl.toPath

  return await level.get

proc getLevel*(player: Player): Future[LoadedLevel] {.async.} =
  ## Load loaded level for player
  return await player.loadedLevel.get

proc getDefault*(): Future[LoadedLevel] {.async.} =
  ## Load default map. If it doesn't exist, create a flatgrass
  if not fileExists(Default.toPath):
    debug "creating flatgrass,", path = Default.toPath
    discard await Default.createFlatgrass(128, 64, 128)
  return await Default.get

proc sendTo*(player: Player, lvl: LoadedLevel) {.async.} =
  ## Sets player's map. Does not perform any network activities
  if player.loadedLevel != "":
    let loaded = await get player.loadedLevel
    loaded.playersT.del player.id.int
    if loaded.playersT.len == 0 and loaded.lvl.name != Default:
      await close loaded.lvl
      levels.del loaded.lvl.name

  lvl.playersT[player.id.int] = player
  player.loadedLevel = lvl.lvl.name
  debug "set player's level", player = player.name, level = lvl.lvl.name

proc sendToDefault*(player: Player): Future[LoadedLevel] {.async.} =
  let lvl = await getDefault()
  await player.sendTo lvl
  return lvl

iterator players*(lvl: LoadedLevel): Player =
  for _, player in lvl.playersT:
    if player != nil:
      yield player

iterator allPlayers*(): Player =
  for _, level in levels:
    for _, player in level.playersT:
      if player != nil:
        yield player
