import std/[asyncnet, asyncdispatch, sequtils]
import datatypes, level

type
  Rank* = enum
    Banned = "&8"
    Guest = "&7"
    Member = "&f"
    Moderator = "&2" 
    Admin = "&4"

  Position* = object
    x*, y*, z*:   Short
    yaw*, pitch*: Byte

  PlayerSpec* = object
    id*:     Byte
    ip*:     String
    name*:   String
    rank*:   Rank
    pos*:    ref Position
    socket*: AsyncSocket
    # the boolean returned here signifies "done"
    setBlockCb*: proc(player_id: int, players: seq[Player], x, y, z: Short,
                      blocktype: Byte, level: Level): Future[bool]
    loadedLevel*: string
    toggled*:     bool
    extra*:       string

  Player* = ref PlayerSpec

func valid*(players: seq[Player]): seq[Player] =
  players.filterIt(not it.isNil)

func hasRank*(player: Player, rank: Rank): bool =
  player.rank >= rank

