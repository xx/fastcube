import std/times

# todo: not cross-compatible
const buildVersion* = staticExec "git describe --tags"
let startTime = now()

template range(comparable: Natural, format: string) =
  if comparable > 0:
    if result != "": result.add(", ")
    result.add($comparable & " " & format)
    if comparable > 1: result.add "s"

proc uptime*(): string =
  let up = between(startTime, now())

  range(up.years, "year")
  range(up.months, "month")
  range(up.days, "day")
  range(up.hours, "hour")
  range(up.minutes, "minute")
  range(up.seconds, "second")
