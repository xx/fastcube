import datatypes
import flatty/binny

type
  PlayerIdent* = object
    # Sent by a player joining a server with relevant information. The protocol
    # version is 0x07, unless you're using a client below 0.28.
    protocolVersion*: Byte
    username*, verificationKey*: String
    unused*: ByteArray

  SetBlock* = object
    # Sent when a user changes a block. The mode field indicates whether a block
    # was created (0x01) or destroyed (0x00).
    # Block type is always the type player is holding, even when destroying.
    # Client assumes that this command packet always succeeds, and so draws the
    # new block immediately. To disallow block creation, server must send back
    # Set Block packet with the old block type.
    x*, y*, z*: Short
    mode*, blockType*: Byte

  Position* = object
    # Sent frequently (even while not moving) by the player with the player's
    # current location and orientation. Player ID is always -1 (255), referring
    # to itself.
    playerId*: SByte
    x*, y*, z*: FShort
    yaw*, pitch*: Byte

  Message* = object
    # Contain chat messages sent by player. Player ID is always -1 (255),
    # referring to itself.
    playerId*: SByte
    message*: String

proc parsePlayerIdent*(data: string): PlayerIdent =
  var i = 1
  result.protocolVersion = data[i].Byte
  i += sizeof result.protocolVersion
  result.username = unpackString data[i ..< i + StringMax]
  i += StringMax
  result.verificationKey = unpackString data[i ..< i + StringMax]

proc parseSetBlock*(data: string): SetBlock =
  var i = 1
  result.x = cast[Short](swap data.readUint16(i))
  i += sizeof result.x
  result.y = cast[Short](swap data.readUint16(i))
  i += sizeof result.y
  result.z = cast[Short](swap data.readUint16(i))
  i += sizeof result.z
  result.mode = data[i].Byte
  i += sizeof result.mode
  result.blockType = data[i].Byte

proc parsePosition*(data: string): Position =
  var i = 1
  result.playerId = cast[Sbyte](data[i])
  i += sizeof result.playerId
  result.x = swap data.readInt16(i)
  i += sizeof result.x
  result.y = swap data.readInt16(i)
  i += sizeof result.y
  result.z = swap data.readInt16(i)
  i += sizeof result.z
  result.yaw = data[i].Byte
  i += sizeof result.yaw
  result.pitch = data[i].Byte

proc parseMessage*(data: string): Message =
  var i = 1
  result.playerId = cast[SByte](data[i])
  i += sizeof result.playerId
  result.message = unpackString data[i ..< i + StringMax]
